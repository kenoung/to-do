Rails.application.routes.draw do
  resources :todo_lists do
    resources :todo_items do
      member do
        patch :complete
      end
    end
  end
  
  root "todo_lists#index"
  get 'tags/:tag' => 'todo_items#index', as: :tag
end
