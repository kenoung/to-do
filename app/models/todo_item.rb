class TodoItem < ActiveRecord::Base
  acts_as_taggable
  belongs_to :todo_list
  validates :content, length: { in: 1..255 }
  
  def completed?
    !completed_at.blank?
  end
end
